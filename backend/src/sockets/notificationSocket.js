const Music = require("../models/musicModel")
const Notification = require("../models/notificationModel")
const User = require("../models/userModel")

const notificationSocket = async (io, musicId, userId) => {
    try {
        const music = await Music.findById(musicId);
        const authorId = music.user_id;
        if (authorId == userId) {
            return
        }

        const newNotification = new Notification({
            music_id: musicId,
            user_id: userId,
        })
        await newNotification.save();
        const musicname = music.music_name;
        const userData = await User.findById(userId);

        const populatedNoti = {
            ...newNotification.toObject(),
            musicname: musicname,
            username: userData.username,
            authorId: authorId
        };

        io.emit("new_comment_notification", { newNotification: populatedNoti })
    }
    catch (err) {
        console.log(err);
    }
}

module.exports = notificationSocket;