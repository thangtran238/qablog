const socketIo = require('socket.io');
const commentSocket = require('./commentSocket');

const initializeSocket = (server) => {
    const io = socketIo(server, {
        cors: {
            origin: "http://localhost:3000",
            credentials: true,
        }
    });
    commentSocket(io);

    return io;
}

module.exports = initializeSocket;