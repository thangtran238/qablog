const Comment = require('../models/commentModel');
const User = require('../models/userModel');
const Music = require("../models/musicModel");
const notificationSocket = require('./notificationSocket');

const commentSocket = (io) => {
    io.on("connection", async (socket) => {
        console.log("A user connected");

        socket.on("join_comment", async ({ musicId }) => {
            try {
                socket.join(musicId)
                console.log(`User joined comment music id: ${musicId}`);
                let existingComment = await Comment.find({ music_id: musicId }).populate("user_id", "username").exec()
                socket.emit("existing_comments", { existingComment })
            }
            catch (err) {
                console.log(err);
            }
        })

        socket.on("new_comment", async ({ musicId, commentContent, userId }) => {
            try {
                const checkMusic = await Music.findById(musicId);
                if (!checkMusic) {
                    console.log("Music not found");
                    return
                }
                const user = await User.findById(userId, 'username');

                if (!user) {
                    console.log("User not found");
                    return;
                }

                const newComment = new Comment({
                    comment_content: commentContent,
                    user_id: userId,
                    music_id: musicId
                })
                await newComment.save()

                const populatedComment = {
                    ...newComment.toObject(),
                    user_id: user
                };

                await notificationSocket(io, musicId, userId)

                io.to(musicId).emit("broadcast_comment", { comment: populatedComment });

            }
            catch (err) {
                console.log(err);
            }
        })

        socket.on("disconnect", () => {
            console.log("User disconnected");
        })
    })
}
module.exports = commentSocket;