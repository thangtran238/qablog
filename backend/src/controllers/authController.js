const User = require("../models/userModel");
const bcrypt = require('bcrypt');
const crypto = require("crypto");
const nodemailer = require("nodemailer")
const helpers = require('../helpers/jwt')
const Role = require('../models/roleModel')
const jwt = require('jsonwebtoken')

const authController = {

    // requestRefreshToken
    requestRefreshToken: async (req, res) => {
        // TAKE REFRESH TOKEN TỪ USER.
        const refreshToken = req.cookies.refreshtoken;
        console.log(refreshToken);
        if (!refreshToken) return res.status(401).json("You are not authenticated")

        // const userToken = User.findOne({id: req.})
        jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, async (err, user) => {
            if (err) console.log(err);
            // Create new RefreshToken, refresh access_token
            const userDb = await User.findById(user.userId);
            if (!userDb) {
                return res.status(401).json("User not found");
            }
            const newAccessToken = helpers.generateAccessToken(userDb)
            const newRefreshToken = helpers.generateRefreshToken(userDb);

            const updateRefreshToken = await User.findOneAndUpdate({
                _id: user.userId
            }, {
                $set: {
                    refresh_token: newRefreshToken
                }
            }, {
                new: true
            });
            res.cookie("refreshtoken", newRefreshToken, {
                httpOnly: true,
                secure: false,
                path: "/",
                sameSite: "strict"
            });
            res.cookie('accesstoken', newAccessToken, {
                secure: false,
                path: "/",
                sameSite: "strict"
            })

            res.status(200).json({
                accessToken: newAccessToken
            });
        })
    },

    //REGISTER
    registerUserSendEmail: async (req, res) => {
        try {
            const existingUsername = await User.findOne({
                username: req.body.username
            });
            if (existingUsername) {
                return res.status(400).json({
                    message: 'Username is already registered'
                });
            }
            const existingUserEmail = await User.findOne({
                email: req.body.email
            });
            if (existingUserEmail) {
                return res.status(400).json({
                    message: 'email is already registered'
                });
            }

            const token = crypto.randomBytes(20).toString('hex');
            userEmail = await req.body.email
            userName = await req.body.username
            await sendCreatePasswordEmail(userEmail, userName, token);
            return res.status(200).json({
                message: "successfully"
            })

        } catch (err) {
            return res.status(500).json(err);
        }
    },


    //REGISTER
    registerUser: async (req, res) => {
        try {
            const password = req.body.newPassword;
            const confirmPassword = req.body.confirmPassword;
            if (password != confirmPassword) {
                return res.status(400).json({
                    message: "Passwords are not the same"
                })
            }

            const salt = await bcrypt.genSalt(10);
            const hashed = await bcrypt.hash(password, salt);
            const newRole = new Role();

            const savedRole = await newRole.save();
            const newUser = new User({
                username: req.body.username,
                password: hashed,
                email: req.body.email,
                role_id: savedRole._id,
            });

            await newUser.save();
            return res.status(200).json({
                message: true
            });
        } catch (err) {
            return res.status(500).json(err);
        }
    },


    //LOGIN 
    loginUser: async (req, res) => {

        try {
            const user = await User.findOne({
                username: req.body.username
            });
            if (user == null) {
                return res.status(404).json({
                    message: "Account is not registered"
                });
            }
            if (user.is_active != "active") {
                return res.status(400).json({
                    message: "The account has been deleted. Please contact email: it.trungdang@gmail.com. We will handle it for you."
                });
            }
            if (!user) {
                return res.status(404).json({
                    message: "Username is incorrect"
                });
            }

            const validPassword = await bcrypt.compare(req.body.password, user.password);

            if (!validPassword) {
                return res.status(404).json({
                    message: "Password is incorrect"
                });
            }
            if (user && validPassword) {
                const accessToken = helpers.generateAccessToken(user);
                const refreshToken = helpers.generateRefreshToken(user);

                // Sử dụng findOneAndUpdate để cập nhật trường refresh_token
                const updatedUser = await User.findOneAndUpdate({
                    username: req.body.username
                }, {
                    refresh_token: refreshToken
                }, {
                    new: true
                });

                await res.cookie("refreshtoken", refreshToken, {
                    httpOnly: true,
                    secure: false,
                    path: '/'
                });
                await res.cookie("accesstoken", accessToken, {
                    secure: false,
                    path: '/'
                });
                const {
                    password,
                    refresh_token,
                    ...others
                } = user._doc;
                return res.status(200).json({
                    ...others
                });
            }
        } catch (err) {
            return res.status(500).json(err);
        }
    },

    //LOGOUT 
    logoutUser: (req, res) => {
        try {
            res.clearCookie('accesstoken');
            res.clearCookie('refreshtoken');
            res.status(200).json({
                message: 'Logout successful'
            });
        } catch (error) {
            return res.status(500).json({
                message: 'Internal Server Error'
            });
        }
    },
    // ForgotPassword
    forgotPassword: async (req, res) => {
        try {
            const user = await User.findOne({
                email: req.body.email
            });
            if (!user) {
                return res.status(404).json({
                    message: "Email is not registered"
                })
            }
            // Tạo token ngẫu nhiên
            const token = crypto.randomBytes(20).toString('hex');

            // Sử dụng updateOne để cập nhật dữ liệu dựa trên điều kiện email
            const timestamp = Date.now() + 60 * 60 * 1000;
            await User.updateOne({
                email: req.body.email
            }, {
                resetPasswordToken: token,
                resetPasswordExpires: timestamp

            });
            const userEmail = req.body.email;

            await sendResetPasswordEmail(userEmail, token);
            return res.status(200).json({
                message: 'Email sent for password reset'
            });
        } catch (err) {
            return res.status(500).json(err);
        }
    },

    //resetPassword
    resetPassword: async (req, res) => {
        try {
            const email = req.body.email
            const token = req.body.token;
            const newPassword = req.body.newPassword;
            const confirmPassword = req.body.confirmPassword;
            // Tìm người dùng với token cụ thể và token chưa hết hạn
            const user = await User.findOne({
                resetPasswordToken: token,
                resetPasswordExpires: {
                    $gt: Date.now()
                },
            });

            if (!user) {
                return res.status(400).json({
                    message: 'Invalid or expired token, please re-enter your email to receive a new Password reset link.'
                });
            }

            if (newPassword != confirmPassword) {
                return res.status(400).json({
                    message: "Passwords are not the same"
                })
            }
            // Đặt lại mật khẩu cho người dùng
            const salt = await bcrypt.genSalt(10)
            const hashed = await bcrypt.hash(req.body.newPassword, salt);
            await User.updateOne({
                email: req.body.email
            }, {
                resetPasswordToken: null,
                resetPasswordExpires: null,
                password: hashed
            });
            return res.status(200).json({
                message: 'Password reset successfully'
            });
        } catch (err) {
            console.error(err);
            return res.status(500).json({
                message: 'A server error has occurred'
            });
        }
    },

}

module.exports = authController;
// Khởi tạo transporter của Nodemailer
const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'it.trungdang@gmail.com',
        pass: 'xogw hqqu xhln jxvu'
    }
});
// function to send email
const sendResetPasswordEmail = async (recipientEmail, token) => {
    try {
        // Tạo nội dung email
        const mailOptions = {
            from: 'it.trungdang@gmail.com',
            to: recipientEmail,
            subject: 'Đặt lại mật khẩu',
            text: `Bạn đã yêu cầu đặt lại mật khẩu tài khoản của bạn.\n
                Vui lòng nhấp vào liên kết sau để đặt lại mật khẩu:\n
                http://localhost:3000/resetpw/email=${recipientEmail}/tokenResetPW=${token}\n
                Liên kết này sẽ hết hạn sau 1 giờ.\n
                Nếu bạn không yêu cầu đặt lại mật khẩu, vui lòng bỏ qua email này.`,
        };

        // Send email without using req and res parameters
        const info = await transporter.sendMail(mailOptions);
    } catch (error) {
        console.error(error);
        throw error;
    }
};

const sendCreatePasswordEmail = async (userEmail, userName, token) => {
    try {
        // Tạo nội dung email
        const mailOptions = {
            from: 'it.trungdang@gmail.com',
            to: userEmail,
            subject: 'Create your password',
            text: `Bạn đang đăng ký tài khoản với username: .\n
                Vui lòng nhấp vào liên kết sau để tạo mật khẩu:\n
                http://localhost:3000/CreatePw/email=${userEmail}/username=${userName}/tokenCreatePW=${token}\n
                Liên kết này sẽ hết hạn sau 1 giờ.`,
        };

        // Send email without using req and res parameters
        const info = await transporter.sendMail(mailOptions);
    } catch (error) {
        console.error(error);
        throw error;
    }
};
