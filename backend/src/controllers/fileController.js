const firebaseConfig = require('../config/firebase/firebase');
const firebaseApp = require('firebase/app');
const firebaseStorage = require('firebase/storage');
const fs = require('fs')
const path = require('path');
const https = require('https');
const Music = require('../models/musicModel');
const axios = require('axios');
firebaseApp.initializeApp(firebaseConfig.firebaseConfig);
const storage = firebaseStorage.getStorage();

class fileController {



    uploadFile = async (req, res) => {
        try {
            const userId = req.body.id;
            const musicName = req.body.music;
            const author = req.body.author;

            const filenameFile = await req.files.filename[0];
            const imageFile = await req.files.image[0];
            const metadata = {
                contentType: 'audio/mpeg'
            };

            const subMetadata = {
                contentType: 'image/jpeg'
            }
            // return res.status(200).json(filenameFile)

            const sanitizedFilename = filenameFile.originalname
                .replace(/[^\x00-\x7F]/g, '')
                .replace(/\s/g, '');

            const sanitizedThumbnail = imageFile.originalname
                .replace(/[^\x00-\x7F]/g, '')
                .replace(/\s/g, '');


            if (!sanitizedFilename || !sanitizedThumbnail) {
                return res.status(400).json({
                    error: 'Invalid filename or thumbnail name.'
                });
            }

            const endpoint = sanitizedFilename.split('.')[1];
            const lastChar = endpoint.charAt(endpoint.length - 1);
            if (lastChar !== '3' || endpoint.toLowerCase() !== 'mp3') {
                return res.status(403).json({
                    message: 'Only receive mp3 file'
                });
            }

            const storageRefFilename =  firebaseStorage.ref(storage, `/music/${sanitizedFilename}`);
            const storageRefThumbnail = firebaseStorage.ref(storage, `/thumbnail/${sanitizedThumbnail}`);



            const snapshotFilename = await firebaseStorage.uploadBytesResumable(storageRefFilename, filenameFile.buffer, metadata);
            const downloadURLFilename = await firebaseStorage.getDownloadURL(snapshotFilename.ref);
            const snapshotThumbnail = await firebaseStorage.uploadBytesResumable(storageRefThumbnail, imageFile.buffer, subMetadata);

            const downloadURLThumbnail = await firebaseStorage.getDownloadURL(snapshotThumbnail.ref);

            const firebaseRes = downloadURLFilename;

            const newMusic = await Music.create({
                music_name: musicName,
                music_author: author,
                music_thumbnail: downloadURLThumbnail,
                user_id: userId,
                download_path: firebaseRes
            })

            if (newMusic) return res.status(200).json(newMusic)

        } catch (error) {
            console.log(error);
            return res.status(400).json({
                error: error.message
            });
        }
    };

    getFile = async (url) => {
        return new Promise((resolve, reject) => {
            const request = https.get(url, (response) => {
                let data = Buffer.from([]);

                response.on('data', (chunk) => {
                    data = Buffer.concat([data, chunk]);
                });

                response.on('end', () => {
                    resolve(data);
                });

                response.on('error', (error) => {
                    reject(error);
                });
            });

            request.on('error', (error) => {
                reject(error);
            });
        });
    };

    downloadFile = async (req, res) => {

        const id = req.params.id;
        if (!id) {
            return res.status(400).json({
                error: 'URL parameter is required'
            });
        }

        try {
            const musicPost = await Music.findById(id).exec();
            if (!musicPost) {
                return res.status(404).json({
                    error: 'Music not found'
                });
            }
            const fileUrl = musicPost.download_path
            const response = await axios.get(fileUrl, { responseType: 'stream' });
            if (!response.data) {
                return res.status(500).json({
                    error: 'Empty or invalid response from the server'
                });
            }
            const filename = fileUrl.split('/').pop().split('?')[0];
            res.setHeader('Content-Disposition', `attachment; filename="${filename}"`);
            res.setHeader('Content-Type', 'audio/mpeg');
            res.setHeader('Content-Length', response.headers['content-length']);
    
            // Pipe the response stream directly to the client's response
            response.data.pipe(res);

        } catch (error) {
            console.error('Error downloading audio:', error.message);
            return res.status(500).json({
                error: 'Internal Server Error'
            });
        }
    };
}

module.exports = new fileController();