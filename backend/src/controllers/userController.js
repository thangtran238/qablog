const User = require("../models/userModel.js");
const bcrypt = require('bcrypt');
const updateUser = async (req, res) => {
    try {
        const newDataUser = req.body;

        const password = newDataUser.password;

        const hashedPassword = await bcrypt.hash(password, 10);

        newDataUser.password = hashedPassword;

        const userId = req.userData.id;

        if (newDataUser.is_active) {
            delete newDataUser.refresh_token;
        }
        if (newDataUser.is_active) {
            delete newDataUser.is_active;
        }

        if (newDataUser.role_id) {
            delete newDataUser.role_id;
        }
        const user = await User.findById(userId);

        if (!user) {
            return res.status(404).send("User not found");
        }
        const updateUser = await User.findOneAndUpdate(
            { _id: userId },
            { $set: newDataUser },
            { new: true }
        );

        if (!updateUser) {
            return res.status(404).send("User can't update");
        }
        return res.status(200).send("User updated successfully")
    }
    catch (err) {
        console.log(err);
        return res.status(500).send("Internal server error");
    }
}

const deleteUser = async (req, res) => {

    const authenticatedUser = req.userData;
    const userid = req.params.id;
    if (authenticatedUser.id !== userid) {
        return res.status(401).send("The account you want delete does not match");
    }
    try {
        const filter = { "_id": userid, "is_active": "active" };
        const update = { $set: { "is_active": "delete" } };
        const result = await User.updateOne(filter, update)
        if (!result) {
            return res.status(404).send("User not found or already deleted");
        }

        return res.status(200).json("Your account has been successfully deleted");
    }
    catch (err) {
        console.log(err);
        return res.status(500).send("Internal server error");
    }
}

module.exports = { updateUser, deleteUser };