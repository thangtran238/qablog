const { default: mongoose } = require("mongoose");
const Music = require("../models/musicModel");

exports.getAllMusic = (async (req, res, next) => {
    try {
        const music = await Music.find().exec();
        return res.status(200).json(music);
    } catch (error) {
        // return res.status(500).json({ error: 'Internal Server Error' });
        console.log(error);
        return res.status(500).json(error);
    }
});

exports.getMusicByID = (async (req, res, next) => {
    try {
        const id = req.params.id;
        if (!mongoose.Types.ObjectId.isValid(id)) {
            return res.status(400).json({ message: 'Invalid music ID' });
        }
        const music = await Music.findById(id);
        if (!music) {
            return res.status(404).json({ message: 'Music not found' });
        }
        return res.status(200).json(music);
    } catch (error) {
        console.error('Error retrieving music by ID:', error);
        return res.status(500).json({ error: 'Internal Server Error' });
    }
});

exports.createNewMusic = (async (req, res, next) => {
    try {
        const { music_name, music_author, music_thumbnail, user_id } = req.body;
        if (!music_name || !music_author || !music_thumbnail || !user_id) {
            return res.status(400).json({ message: 'All fields are required' });
        }
        const newMusic = new Music({
            music_name: music_name,
            music_author: music_author,
            music_thumbnail: music_thumbnail,
            user_id: user_id,
        });
        const savedMusic = await newMusic.save();
        return res.status(201).json(savedMusic);
    } catch (error) {
        console.error('Error creating new music:', error);
        return res.status(500).json({ error: 'Internal Server Error' });
    }
});

exports.updateMusicByID = (async (req, res, next) => {
    try {
        const id = req.params.id;
        if (!mongoose.Types.ObjectId.isValid(id)) {
            return res.status(400).json({ message: 'Invalid music ID' });
        }
        const { music_name, music_author, music_thumbnail, user_id } = req.body;
        if (!music_name && !music_author && !music_thumbnail && !user_id) {
            return res.status(400).json({ message: 'At least one field is required for updating' });
        }
        const music = await Music.findById(id);
        if (!music) {
            return res.status(404).json({ message: 'Music not found' });
        }
        if (music_name) music.music_name = music_name
        if (music_author) music.music_author = music_author
        if (music_thumbnail) music.music_thumbnail = music_thumbnail
        const updatedMusic = await music.save();
        return res.status(200).json(updatedMusic);
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal Server Error' });
    }
});

exports.deleteMusicByID = (async (req, res, next) => {
    try {
        const id = req.params.id;
        if (!mongoose.Types.ObjectId.isValid(id)) {
            return res.status(400).json({ message: 'Invalid music ID' });
        }
        const music = await Music.findById(id);
        if (!music) {
            return res.status(404).json({ message: 'Music not found' });
        }
        await music.deleteOne();
        return res.status(204).send();
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal Server Error' });
    }
});





