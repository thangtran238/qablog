const Music = require("../models/musicModel");
const Notification = require("../models/notificationModel");
const User = require("../models/userModel");

const notification = async (req, res) => {
    try {
        const userId = req.userData._id;

        const musicData = await Music.find({ user_id: userId })

        const musicIds = musicData.map((music) => music._id);

        const notification = await Notification.find({
            music_id: { $in: musicIds },
        })

        const newData = await Promise.all(notification.map(async (noti) => {
            const musicId = noti.music_id;
            const music = await Music.findById(musicId);
            const userData = await User.findById(userId);

            if (music && userData) {
                const musicname = music.music_name;
                const username = userData.username;


                const updatedNoti = {
                    ...noti.toObject(),
                    username,
                    musicname,
                };
                return updatedNoti;

            }

            return noti;
        }))
        return res.status(200).send(newData);

    }
    catch (err) {
        console.log(err);
        return res.status(500).send("Internal server error");
    }
}

module.exports = { notification };