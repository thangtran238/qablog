const mongoose = require('mongoose');

const connect = async () => {
    try {
        await mongoose.connect(`mongodb+srv://thangtran24:${process.env.MONGO_PASSWORD}@cluster0.lofqkd8.mongodb.net/qablog`).then(() => {
            console.log('Connect successfully');
        })
    } catch (error) {
        console.log('Connect fail');
    }
}

module.exports = connect