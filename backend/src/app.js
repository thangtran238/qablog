const express = require('express');
const morgan = require('morgan');
const cookieParse = require('cookie-parser');
const dotenv = require('dotenv');
const cors = require('cors');
const connect = require('./config/db/db')

const route = require('./routes/index')
const http = require('http')
const app = express();
const helmet = require('helmet');
const xss = require('xss-clean');
const initializeSocket = require("./sockets/initializeSocket")
dotenv.configDotenv();
connect();

app.use(morgan());
app.use(helmet());
app.use(cors({
    origin: 'http://localhost:3000',
    credentials: true,
}));
app.use(cookieParse());
app.use(express.json());
app.use(xss());
const server = http.createServer(app);

initializeSocket(server)
route(app);

server.listen(process.env.PORT, () => {
    console.log(`Listening at ${process.env.PORT}`);
})
