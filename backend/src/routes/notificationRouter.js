const express = require('express');
const { notification } = require("../controllers/notificationController.js");

const { checkAuthentication } = require("../middlewares/checkAuthentication.js");

const router = express.Router();

router.get("/", checkAuthentication, notification)

module.exports = router;