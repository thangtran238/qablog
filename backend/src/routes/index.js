const fileRouter = require('./file')
const authRouter = require('./authRouter')
const userRouter = require('./userRouter')
const musicRoutes = require('./musicRoutes')
const notificationRouter = require('./notificationRouter')

function route(app) {
    app.use('/file', fileRouter);
    app.use('/auth', authRouter);
    app.use('/users', userRouter);
    app.use('/music', musicRoutes);
    app.use('/notification', notificationRouter);
}

module.exports = route