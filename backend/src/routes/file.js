const express = require('express');
const fileController = require('../controllers/fileController');
const {
    checkAuthentication
} = require('../middlewares/checkAuthentication')
const multer = require('multer');


const storage = multer.memoryStorage();
const upload = multer({
    storage: storage
});
const fileRouter = express.Router();

fileRouter.post('/upload', upload.fields([{
        name: 'filename'
    },
    {
        name: 'image'
    },
]),checkAuthentication, fileController.uploadFile);
fileRouter.get('/download/:id', fileController.downloadFile);

module.exports = fileRouter;