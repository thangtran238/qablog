const express = require('express');
const { deleteUser, updateUser } = require("../controllers/userController");

const { checkAuthentication } = require("../middlewares/checkAuthentication.js");

const router = express.Router();

router.post("/update", checkAuthentication, updateUser)

router.delete("/delete/:id", checkAuthentication, deleteUser)

module.exports = router;