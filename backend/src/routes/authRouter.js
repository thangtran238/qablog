const authController = require('../controllers/authController')
const express = require('express');
const { verifyAccessToken } = require('../helpers/jwt');
const { checkAuthentication } = require('../middlewares/checkAuthentication');

const AuthRouter = express.Router();

AuthRouter.post("/signin", authController.loginUser);
AuthRouter.post("/registerUserSendEmail", authController.registerUserSendEmail)
AuthRouter.post("/registerUser", authController.registerUser)
AuthRouter.post("/forgotPassword", authController.forgotPassword)
AuthRouter.post("/resetPassword", authController.resetPassword)
AuthRouter.post("/logout", checkAuthentication, authController.logoutUser)
AuthRouter.post('/refresh', authController.requestRefreshToken)



module.exports = AuthRouter;
