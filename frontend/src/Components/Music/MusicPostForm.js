import React, { useState } from 'react';
import Dropzone from 'react-dropzone';
import "./MusicPostForm.css";
import Navbar from '../layout/Navbar';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { createAxios } from '../../createInstance';
import { uploadSuccess } from '../../redux/fileSlice';
import { addNewMusicPost } from '../../redux/apiRequest';
import Cookies from 'js-cookie';
import Loader from '../loader/Loader';


const MusicPostForm = () => {
    const [name, setName] = useState('');
    const [author, setAuthor] = useState('');

    const [thumbnail, setThumbnail] = useState(null);
    const [thumbnailReview, setThumbnailReview] = useState(null);
    const [audioReview, setAudioReview] = useState(null);
    const [audioFile, setAudioFile] = useState(null);

    const currentUser = useSelector((state) => state.auth.login?.currentUser);
    const id = currentUser?._id
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const isFetching = useSelector((state) => state.file.upload?.isFetching);



    const accessToken = Cookies.get('accesstoken');
    let axiosJWT = createAxios(currentUser, dispatch, uploadSuccess)

    const handleThumbnailDrop = (acceptedFiles) => {
        const file = acceptedFiles[0];
        console.log(file);
        if (acceptedFiles[1]) {
            alert('Waning: You should upload only one thumbnail')
        }
        setThumbnail(file);
        setThumbnailReview(URL.createObjectURL(file))
    };

    const handleAudioFileDrop = (acceptedFiles) => {
        const file = acceptedFiles[0];
        if (acceptedFiles[1]) {
            alert('Waning: You should upload only one audio file')
        }

        setAudioFile(file);
        setAudioReview(URL.createObjectURL(file))
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append('id', id);
        formData.append('music', name);
        formData.append('author', author);
        formData.append('image', thumbnail);
        formData.append('filename', audioFile);

        try {
            await addNewMusicPost(dispatch, accessToken, formData, navigate, axiosJWT);
        } catch (error) {
            console.error(error);
        }
    };

    const handleClear = (e) => {
        e.preventDefault();
        setName('');
        setAuthor('');
        setThumbnail(null);
        setThumbnailReview(null);
        setAudioReview(null);
        setAudioFile(null);
    };

    return (
        <>
           
        {isFetching ? ( <Loader type={'loader'}/>) : (
            <div>
             <Navbar></Navbar>
            <div className="col-md-6 offset-md-3 mt-5">
                <br />
                <h1>Upload form</h1>
                <form acceptCharset="UTF-8" method="POST" encType="multipart/form-data">
                    <div className="form-group">
                        <label htmlFor="exampleInputName">Music name</label>
                        <input type="text" name="musicname" className="form-control" placeholder="Enter the music name" required 
                        onChange={(e) => setName(e.target.value)}
                        value= {name}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Music author</label>
                        <input type="email" name="author" value={author} className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Music author" required 
                        onChange={(e) => setAuthor(e.target.value)}
                        />
                    </div>
                    <hr />
                    <div className="form-group mt-3">
                        {!thumbnailReview ? (
                            <Dropzone onDrop={handleThumbnailDrop} accept="jpg">
                                {({ getRootProps, getInputProps }) => (
                                    <div {...getRootProps()} style={dropzoneStyle}>
                                        <input {...getInputProps()} type='file' name='image' />
                                        <p>Drag & drop a music thumbnail here, or click to select one</p>
                                    </div>
                                )}
                            </Dropzone>
                        ) : (
                            <img src={thumbnailReview} alt="Thumbnail" style={thumbnailStyle} />
                        )}
                    </div>
                    <div className="form-group mt-3">
                        {!audioReview ? (
                            <Dropzone onDrop={handleAudioFileDrop} accept="mp3">
                                {({ getRootProps, getInputProps }) => (
                                    <div {...getRootProps()} style={dropzoneStyle}>
                                        <input {...getInputProps()} type='file' name='filename' />
                                        <p>Drag & drop an audio file here, or click to select one</p>
                                    </div>
                                )}
                            </Dropzone>
                        ) : null}
                        {audioFile && (
                            <audio controls style={audioStyle}>
                                <source src={audioReview} type="audio/mp3" />
                                Your browser does not support the audio element.
                            </audio>
                        )}
                    </div>
                    <button type="submit" className="btn btn-primary" onClick={handleSubmit}>Submit</button>
                    <button type="submit" className="btn btn-danger" onClick={handleClear}>Clear all</button>
                </form>
            </div>
            </div>
        )}
            

        </>
    );
};

const dropzoneStyle = {
    border: '2px dashed #00ff00',
    borderRadius: '4px',
    padding: '20px',
    textAlign: 'center',
    cursor: 'pointer',
    margin: '10px 0',
};

const thumbnailStyle = {
    maxWidth: '100px',
    maxHeight: '100px',
    margin: '10px 0',
};

const audioStyle = {
    margin: '10px 0',
};

export default MusicPostForm;
