import React, { useState, useEffect, useContext } from "react";
import "./DetailMusic.css";
import { Link } from "react-router-dom";
import Navbar from "../layout/Navbar";
import Foot from "../layout/Foot";
import Comment from "../comment/Comment";
import { useLocation } from "react-router-dom";
import { NotificationContext } from "../../context/notificationProvider";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft, faEllipsisV } from "@fortawesome/free-solid-svg-icons";
import { faDownload } from "@fortawesome/free-solid-svg-icons";

export default function DetailMusic() {
  const location = useLocation();
  const id = new URLSearchParams(location.search).get("id");

  const [showInput, setShowInput] = useState(true);
  const [userId, setUserId] = useState("");
  const [commentContent, setCommentContent] = useState("");
  const [data, setData] = useState([]);
  const [existingComments, setExistingComments] = useState([]);
  const { socket } = useContext(NotificationContext);
  const REACT_APP_PORT_API = process.env.REACT_APP_PORT_API;
  const REACT_APP_PORT = process.env.REACT_APP_PORT;

  useEffect(() => {
    const currentUser = localStorage.getItem("currentUser");
    if (currentUser) {
      const userObject = JSON.parse(currentUser);
      setUserId(userObject._id);
      if (id) {
        socket.emit("join_comment", { musicId: id });
      }
    }
    socket.on("existing_comments", ({ existingComment }) => {
      setExistingComments(existingComment);
    });
    socket.on("broadcast_comment", ({ comment }) => {
      setExistingComments((prevComments) => [...prevComments, comment]);
    });
    return () => {
      socket.off("existing_comments");
      socket.off("broadcast_comment");
    };
  }, [id]);

  useEffect(() => {
    fetch(`${REACT_APP_PORT_API}/music/${id}`)
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        setData(data);
      });
  }, [id]);

  const handleCommentSubmit = (event) => {
    event.preventDefault();
    if (commentContent !== "") {
      socket.emit("new_comment", {
        musicId: id,
        commentContent: commentContent,
        userId: userId,
      });
      setCommentContent("");
    } else {
      alert("Please enter a comment!");
    }
  };

  const handleCommentLinkClick = () => {
    setShowInput((prevShowInput) => !prevShowInput);
  };

  useEffect(() => {
    fetch(`${REACT_APP_PORT_API}/music/${id}`)
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        setData(data);
      });
  }, []);

  return (
    <>
      <Navbar />
      <div className="detailMusic">
        <div className="detailSongsection ">
          {/* <div className="player">
          <div className="imgbox">
            <img src={data.music_thumbnail} />
          </div>
          <audio controls src={`${data.download_path}`}></audio>
        </div> */}

          <div class="iphone neu d-flex flex-column justify-content-around">
            <div class="title">
              <FontAwesomeIcon icon={faChevronLeft} />
              <div>NOW PLAYING</div>
              <FontAwesomeIcon icon={faEllipsisV} />
            </div>
            <div class="album-cover">
              <img src={data.music_thumbnail} />
            </div>
            <div>
              <audio controls src={`${data.download_path}`}></audio>
            </div>
          </div>
        </div>
        <div className="commentSection">
          <h2 class="song-title mb-3">{data.music_name}</h2>
          <h4 class="artist-title">{data.music_author}</h4>
          <div className="actions">
            <Link to={`/detailmusic?id=${id}`} className="like-btn">
              &#x1F497;
            </Link>
            <Link
              to={`/detailmusic?id=${id}`}
              className="like-btn"
              onClick={handleCommentLinkClick}
            >
              Comment
            </Link>
            <div className="like-btn">
              <FontAwesomeIcon icon={faDownload} />
            </div>
          </div>
          {showInput && (
            <Comment
              existingComments={existingComments}
              handleCommentSubmit={handleCommentSubmit}
              setCommentContent={setCommentContent}
              commentContent={commentContent}
            />
          )}
        </div>
      </div>
      <Foot />
    </>
  );
}
