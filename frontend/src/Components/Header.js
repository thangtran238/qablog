import React from "react";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../redux/authSlice";
import Cookies from "js-cookie";

export default function Header() {
  const dispatch = useDispatch();
  const currentUser = useSelector((state) => state.auth.login?.currentUser);
  console.log(currentUser);
  const REACT_APP_PORT = process.env.REACT_APP_PORT;

  const handleLogout = () => {
    dispatch(logout());
    console.log(logout);
    Cookies.remove("refreshToken");
    Cookies.remove("accessToken");
    window.location = "/login";
  };

  return (
    <>
      <header className="header-section clearfix">
        <Link to={`${REACT_APP_PORT}`} className="site-logo">
          <img src="img/favicon.ico" alt="" />
        </Link>
        <div className="header-right">
          <Link to="#" className="hr-btn">
            Help
          </Link>
          <span>|</span>

          {!currentUser || !Cookies.get("accessToken") ? (
            <div className="user-panel">
              <Link to="/login" className="login">
                Login
              </Link>
              <Link to="/signup" className="register">
                Create an account
              </Link>
            </div>
          ) : (
            <div className="user-panel">
              <span>Welcome, {currentUser.username}!</span>
              <a
                style={{ color: "white", cursor: "pointer" }}
                onClick={handleLogout}
                className="hi"
              >
                Logout
              </a>
            </div>
          )}
        </div>
        <ul className="main-menu">
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="#">About</Link>
          </li>
          <li>
            <Link to="/songs">Songs</Link>
          </li>
          <li>
            <Link to="/">Pages</Link>
            <ul className="sub-menu">
              <li>
                <Link to="/category">Category</Link>
              </li>
              <li>
                <Link to="/playlist">My Playlist</Link>
              </li>
              <li>
                <Link to="/artist">Artist</Link>
              </li>
              <li>
                <Link to="/blog">Blog</Link>
              </li>
              <li>
                <Link to="/contact">Contact</Link>
              </li>
            </ul>
          </li>
          <li>
            <Link to="/blog">News</Link>
          </li>
          <li>
            <Link to="/contact">Contact</Link>
          </li>
        </ul>
      </header>
    </>
  );
}
