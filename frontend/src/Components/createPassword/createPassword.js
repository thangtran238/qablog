import React, { useState } from "react";
import { useNavigate, Link, useParams } from "react-router-dom";
import { useDispatch } from "react-redux";
import './createPassword.css'
import { RegisterUser } from "../../redux/apiRequest";


const CreatePassword = () => {
    const { email, username, tokenCreatePW } = useParams();
    const [newPassword, setnewPassword] = useState("");
    const [confirmPassword, setconfirmPassword] = useState("")
    const dispatch = useDispatch();
    const navigate = useNavigate();


    const getEmailResetPW = email.split("=")[1];
    const getUsername = username.split("=")[1];
    console.log(getEmailResetPW);
    console.log(getUsername);



    const handleCreatePassword = async (e) => {
        console.log(newPassword);
        console.log(confirmPassword);

        e.preventDefault();
        if (newPassword !== confirmPassword) {
            alert("Passwords are not the same1111")
            return;
        }
        const CreatePasswordData = {
            newPassword: newPassword,
            confirmPassword: confirmPassword,
            email: getEmailResetPW,
            username: getUsername
        }

        const result = await RegisterUser(CreatePasswordData, dispatch);
        if (result.success === true) {
            alert("Create a new account Success. Please log in to confirm your account.")
            navigate('/login')
        } else {
            alert(result.error)
        }
    }

    return (
        <div>
            <section>
                <div className="color" />
                <div className="color" />
                <div className="color" />
                <div className="box">
                    <div className="square" />
                    <div className="square" />
                    <div className="square" />
                    <div className="square" />
                    <div className="square" />
                    <div className="container">
                        <div className="container">
                            <div id="signupForm" className="form form2">
                                <h2>Create A New Password Form</h2>
                                <form>
                                    <div className="emailName">
                                        <h4>Hello: {getEmailResetPW}</h4>
                                    </div>
                                    <div className="inputBox">
                                        <input type="password" placeholder="New Password" onChange={(e) => setnewPassword(e.target.value)} />
                                    </div>
                                    <div className="inputBox">
                                        <input type="password" placeholder="Confirm Password" onChange={(e) => setconfirmPassword(e.target.value)} />
                                    </div>
                                    <div className="inputBox">
                                        <input type="submit" onClick={handleCreatePassword} value="Submit" />
                                    </div>
                                    <p className="forget" id="login">
                                        Don't have an account?
                                        <a href="/signup">Sign up</a>
                                    </p>
                                    <p className="forget" id="login">
                                        Already Have a account?
                                        <Link to="/login">Log In</Link>
                                    </p>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
}

export default CreatePassword

