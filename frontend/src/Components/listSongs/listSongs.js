import Navbar from "../layout/Navbar";
import Foot from "../layout/Foot";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";
import { getAllMusic } from "../../redux/apiRequest";
import { useDispatch, useSelector } from "react-redux";
import { createAxios } from "../../createInstance";
import { downloadSuccess } from "../../redux/fileSlice";
import { downloadMusic } from "../../redux/apiRequest";
import Pagination from "../pagination/Pagination";
import Player from "./Player";
import Cookies from 'js-cookie';
import "./listSongs.css";
export default function ListSongs() {
  const [data, setData] = useState([]);
  const dispatch = useDispatch();
  const currentUser = useSelector((state) => state.auth.login?.currentUser);
  const accessToken = Cookies.get("accesstoken");
  let axiosJWT = createAxios(currentUser, dispatch, downloadSuccess);
  const isFetching = useSelector((state) => state.file.download?.isFetching);

  const [songsData, setSongsData] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(4);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_PORT_API}/music/`, { credentials: "include" })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        setData(data);
      });
  }, [data.length]);

  const handleDownload = async (id, name) => {
    const result = await downloadMusic(
      dispatch,
      accessToken,
      id,
      name,
      axiosJWT
    );
    if (result) {
      console.log("Success");
    }
  };

  const lastPostIndex = currentPage * postsPerPage;
  const firstPostIndex = lastPostIndex - postsPerPage;
  const currentPosts = data.slice(firstPostIndex, lastPostIndex);

  return (
    <>
      <Navbar></Navbar>
      <div
        className={`category-section spad ${
          isFetching ? "pointer-cursor" : ""
        }`}
      >
        <div className="container-fluid">
          <div className="section-title">
            <h2>Playlist</h2>
          </div>
          <div>
            <div className="category-links">
              <Link to="#" className="active">
                All musics
              </Link>
              <Link to="/upload-music">Post your own</Link>
            </div>
          </div>
          <div className="category-items">
            <div className="row">
              <div className="col-md-4">
                <div className="category-item">
                  <img src="img/playlist/9.jpg" alt="" />
                  <div className="ci-text">
                    <h4>Micke Smith</h4>
                    <p>Live from Madrid</p>
                  </div>
                  <a href="artist.html" className="ci-link">
                    <i className="fa fa-play" />
                  </a>
                </div>
              </div>
              <div className="col-md-4">
                <div className="category-item">
                  <img src="img/playlist/2.jpg" alt="" />
                  <div className="ci-text">
                    <h4>Micke Smith</h4>
                    <p>Live from Madrid</p>
                  </div>
                  <a href="artist.html" className="ci-link">
                    <i className="fa fa-play" />
                  </a>
                </div>
              </div>
              <div className="col-md-4">
                <div className="category-item">
                  <img src="img/playlist/7.jpg" alt="" />
                  <div className="ci-text">
                    <h4>Micke Smith</h4>
                    <p>Live from Madrid</p>
                  </div>
                  <a href="artist.html" className="ci-link">
                    <i className="fa fa-play" />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="container-fluid gif">
        {currentPosts.length > 0 ? (
            currentPosts.map((dataMusic, i) => (
  <div className="col" key={i}>
    <Player
      id={dataMusic._id}
      name={dataMusic.music_name}
      author={dataMusic.music_author}
      thumb={dataMusic.music_thumbnail}
      src={dataMusic.download_path}
      handleDownload={() =>
        handleDownload(dataMusic._id, dataMusic.music_name)
      }
    />
  </div>
        ))) :(
          <div className="container-fluid">
            <p className="text-light">Chua co nhac dau fen oi</p>
            </div>

        )}
        <div className="site-pagination pt-5 mt-5">
            <Pagination
              totalPosts={data.length}
              postsPerPage={postsPerPage}
              setCurrentPage={setCurrentPage}
              currentPage={currentPage}
            />
          </div>
          
        </div>
       
      <Foot />
    </>
  );
}
