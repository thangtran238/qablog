import React from 'react'
import { Link } from 'react-router-dom'
import './Player.css'
export default function Player({id, name, author, thumb, src, handleDownload}) {
  return (
<div className="">
  <div className="player d-flex p-6 mx-auto m-3">
    <div className="d-flex p-2 col-2">
    <Link to={`/detailmusic?id=${id}`}>
    <img
        className=" art rounded-lg w-48 h-48"
        style={{objectFit: 'cover'}}
        width={150}
        height={150}
        src={thumb}
        alt=""
      />
      </Link>

     
    </div>
    <div className="d-flex  align-items-center col-8">
    <div className="mx-2 w-100 h-100 d-flex flex-column justify-content-around">
      <div className='ml-3'>
        <div className="text-uppercase font-weight-bold ">
        <Link to={`/detailmusic?id=${id}`} className='text-light'>
        {name}

      </Link>
      </div>

        <div className="font-italic text-secondary font-weight-light">
          {author}
        </div>
      </div>
      
      <div className="d-flex align-items-center justify-content-center mt-2">
        <audio controls controlsList='nodownload' src={src}/>
      </div>
    </div>
    </div>

    <div className="d-flex flex-column justify-content-around align-items-end pr-4 col-2">
      <Link to={`/detailmusic?id=${id}`}>
        <img src="img/icons/p-1.png" alt="" />
      </Link>
      <Link to="">
        <img src="img/icons/p-2.png" alt="" onClick={handleDownload} />
      </Link>
    </div>



  </div>
</div>

  

  )
}
