import React, { useState, useEffect } from "react";
import "./Signup.css";
import { useNavigate, Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { RegisterUserSendEmail } from "../../redux/apiRequest";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function Signup() {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const dispatch = useDispatch();
  const [showForm, setShowForm] = useState(true);


  const handleRegister = async (e) => {
    e.preventDefault();
    toast.success("Please wait a moment...", {
      autoClose: 1500,
    });
    const registerData = {
      username: username,
      email: email,
    }
    const result = await RegisterUserSendEmail(registerData, dispatch);
    console.log(result);
    if (result.success) {
      setShowForm(false)

    } else {
      alert(result.error)
    }
  }


  return (
    <>
      <div>
        <section>
          <div className="color" />
          <div className="color" />
          <div className="color" />
          <div className="box">
            <div className="square" />
            <div className="square" />
            <div className="square" />
            <div className="square" />
            <div className="square" />
            <div className="container">
              <div className="container">
                <div id="signupForm" className="form form2">
                  <h2>Sign Up Form</h2>
                  {showForm ? (
                    <form>
                      <div className="inputBox">
                        <input type="text" placeholder="Username" onChange={(e) => setUsername(e.target.value)} />
                      </div>
                      <div className="inputBox">
                        <input type="email" placeholder="E - Mail" onChange={(e) => setEmail(e.target.value)} />
                      </div>
                      <div className="inputBox">
                        <input type="submit" onClick={handleRegister} value="Sign Up" />
                      </div>
                      <p className="forget">
                        Forgot Password ?<Link to="/forgot-password">Click Here</Link>
                      </p>
                      <p className="forget" id="login">
                        Already Have a account?
                        <Link to="/login">Log In</Link>
                      </p>
                    </form>
                  ) : (
                    <h4 style={{ color: "#FFFF" }}>Please check email to create a new password</h4>
                  )}
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <ToastContainer />
    </>
  );
}
