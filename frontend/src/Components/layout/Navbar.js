import React, { useContext, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Cookies from "js-cookie";
import { NotificationContext } from "../../context/notificationProvider";
import { Link, useNavigate } from "react-router-dom";
import { logoutSuccess, loginSuccess } from "../../redux/authSlice";
import { createAxios } from "../../createInstance";
import { logOutUser } from "../../redux/apiRequest";
import './Style.css';

import { FaBell } from "react-icons/fa";


export default function Navbar() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const currentUser = useSelector((state) => state.auth.login?.currentUser);
  const { socket, notification, setNotification } = useContext(NotificationContext);
  const accessToken = Cookies.get('accesstoken')
  const REACT_APP_PORT_API = process.env.REACT_APP_PORT_API; 

  let axiosJWT = createAxios(currentUser, dispatch, logoutSuccess)


  useEffect(() => {
    const storedUser = localStorage.getItem("currentUser");
    if (storedUser) {
      fetch(`${REACT_APP_PORT_API}/notification/`,{ credentials: "include" })
        .then((res) => {
          return res.json();
        })
        .then((data) => {
          console.log("noti", data);
          setNotification(data);
        });
      dispatch(loginSuccess(JSON.parse(storedUser)));

    }
  }, [dispatch]);

  const handleLogout = () => {

    logOutUser(dispatch, accessToken, navigate, axiosJWT);

  };

  useEffect(() => {
    socket.on('new_comment_notification', ({ newNotification }) => {
      console.log("data noti", newNotification);
      if (currentUser._id === newNotification.authorId) {
        setNotification((prevNotifications) => [newNotification, ...prevNotifications]);
      }
    });
    return () => {
      socket.off('new_comment_notification');
    };
  }, [])

  const [showNotification, setShowNotification] = useState(false);

  const handleNotificationHover = () => {
    setShowNotification(true);
  };

  const handleNotificationLeave = () => {
    setShowNotification(false);

  };

  return (
    <>
      <header className="header-section clearfix">
        <Link to={'/'} className="site-logo">
          <div className="logo"></div>
        </Link>
        <div className="header-right">
          <Link to="#" className="hr-btn">
            Help
          </Link>
          <span>|</span>

          {!currentUser || !Cookies.get("accesstoken") ? (
            <div className="user-panel">
              <Link to="/login" className="login">
                Login
              </Link>
              <Link to="/signup" className="register">
                Create an account
              </Link>
            </div>
          ) : (
            <div className="user-panel">
              <span
                onMouseEnter={handleNotificationHover}
                onMouseLeave={handleNotificationLeave}
              >
                <span className="number">{notification.length}</span>
                <FaBell />
                <div className="cont" >
                  {notification.length ? (
                    notification.map(noti => (
                      <div className="sec new" key={noti._id}>
                        <div className="profCont">
                          <img
                            className="profile"
                            src="https://c1.staticflickr.com/4/3725/10214643804_75c0b6eeab_b.jpg"
                          />
                        </div>
                        <div className="txt">
                          {noti.username} commented on your post: "{noti.musicname}"
                        </div>
                        <div className="txt sub">{noti.created_at}</div>
                      </div>
                    ))
                  ) : (
                    <div className="sec new">
                      <div style={{ color: "black", cursor: "pointer" }}>No New Notification</div>
                    </div>
                  )}
                </div>
              </span>
              <span>Welcome, {currentUser.username}!</span>
              <Link
                style={{ color: "white", cursor: "pointer" }}
                onClick={handleLogout}
                className="hi"
              >
                Logout
              </Link>
            </div>
          )}
        </div>
        <ul className="main-menu">
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="#">About</Link>
          </li>
          {
            !currentUser || !Cookies.get("accesstoken") ? (
              null
            ) : (
              <li>
                <Link to="/songs">Songs</Link>
              </li>
            )
          }


          <li>
            <Link to="/">Pages</Link>
            <ul className="sub-menu">
              <li>
                <Link to="/category">Category</Link>
              </li>
              <li>
                <Link to="/playlist">My Playlist</Link>
              </li>
              <li>
                <Link to="/artist">Artist</Link>
              </li>
              <li>
                <Link to="/blog">Blog</Link>
              </li>
              <li>
                <Link to="/contact">Contact</Link>
              </li>

            </ul>
          </li>
          <li>
            <Link to="/blog">News</Link>
          </li>
          <li>
            <Link to="/contact">Contact</Link>
          </li>
        </ul>
      </header>
    </>
  );
}
