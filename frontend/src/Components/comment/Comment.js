import React from "react";
import "./comment.css";

export default function Comment({
  existingComments,
  handleCommentSubmit,
  commentContent,
  setCommentContent,
}) {
  const handleCommentChange = (event) => {
    setCommentContent(event.target.value);
  };

  return (
    <>
      <div className="comments">
        {existingComments.map((comment) => (
          <div className="comment" key={comment._id}>
            <div className="comment-author">{comment.user_id.username}</div>
            <div className="comment-content">{comment.comment_content}</div>
          </div>
        ))}
      </div>
      <div className="comment-input">
        <form onSubmit={handleCommentSubmit}>
          <input
            type="text"
            value={commentContent}
            onChange={handleCommentChange}
            placeholder="Type your comment..."
          />
          <button class="btns btn-masterful">
            <span class="icon">&#x1F3B5;</span>
            <span class="btn-txt">Submit</span>
          </button>
        </form>
      </div>
    </>
  );
}
