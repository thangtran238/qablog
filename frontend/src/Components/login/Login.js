import React, { useEffect, useState } from "react";
import './login.css'
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { loginUser } from "../../redux/apiRequest";
import Loader from "../loader/Loader";

const Login = () => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const isFetching = useSelector((state) => state.auth.login?.isFetching);

    const handleLogin = async (e) => {
        e.preventDefault();
        const newUser = {
            username: username,
            password: password,
        }

        const result = await loginUser(newUser, dispatch)
        if (result.success === true) {
            navigate('/')
        } else {
            alert(result.error)
        }
    }

    return (
        <div>
            <section>
                <div className="color" />
                <div className="color" />
                <div className="color" />
                <div className="box">
                    <div className="square" />
                    <div className="square" />
                    <div className="square" />
                    <div className="square" />
                    <div className="square" />
                    <div className="container">
                        <div className="container">
                            {isFetching ? (
                                <Loader />
                            ) : (
                                <div id="signupForm" className="form form2">
                                    <h2>Sign In Form</h2>
                                    <form>
                                        <div className="inputBox">
                                            <input type="text" placeholder="Username"
                                                onChange={(e) => setUsername(e.target.value)}
                                            />
                                        </div>
                                        <div className="inputBox">
                                            <input type="password" placeholder="Password"
                                                onChange={(e) => setPassword(e.target.value)}
                                            />
                                        </div>
                                        <div className="inputBox">
                                            <input type="submit" onClick={handleLogin} value="Sign In" />
                                        </div>
                                        <p className="forget">
                                            Forgot Password ?<Link to="/forgot-password">Click Here</Link>
                                        </p>
                                        <p className="forget" id="login">
                                            Don't have an account?
                                            <a href="/signup">Sign up</a>
                                        </p>
                                    </form>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
};

export default Login;
