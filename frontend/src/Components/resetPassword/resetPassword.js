import React, { useState } from "react";
import { useNavigate, Link, useParams } from "react-router-dom";
import { useDispatch } from "react-redux";
import './resetPassword.css';
import { resetPassword } from "../../redux/apiRequest";


const ResetPassword = () => {
    const { email, tokenResetPW } = useParams();
    const [newPassword, setnewPassword] = useState("");
    const [confirmPassword, setconfirmPassword] = useState("")
    const dispatch = useDispatch();
    const navigate = useNavigate();


    const getEmailResetPW = email.split("=")[1];
    const getTokenResetPW = tokenResetPW.split("=")[1];


    const handleResetPassword = async (e) => {
        e.preventDefault();
        if (newPassword !== confirmPassword) {
            alert("Passwords are not the same")
            return;
        }
        const resetPasswordData = {
            newPassword: newPassword,
            confirmPassword: confirmPassword,
            token: getTokenResetPW,
            email: getEmailResetPW
        }

        const result = await resetPassword(resetPasswordData, dispatch);
        if (result.success === true) {
            alert("Reset Password Success. Please log in to confirm your account.")
            navigate('/login')
        } else {
            alert(result.error)
            navigate('/forgot-password')
        }
    }

    return (
        <div>
            <section>
                <div className="color" />
                <div className="color" />
                <div className="color" />
                <div className="box">
                    <div className="square" />
                    <div className="square" />
                    <div className="square" />
                    <div className="square" />
                    <div className="square" />
                    <div className="container">
                        <div className="container">
                            <div id="signupForm" className="form form2">
                                <h2>Reset Password Form</h2>
                                <form>
                                    <div className="emailName">
                                        <h4>Hello: {getEmailResetPW}</h4>
                                    </div>
                                    <div className="inputBox">
                                        <input type="password" placeholder="New Password" onChange={(e) => setnewPassword(e.target.value)} />
                                    </div>
                                    <div className="inputBox">
                                        <input type="password" placeholder="Confirm Password" onChange={(e) => setconfirmPassword(e.target.value)} />
                                    </div>
                                    <div className="inputBox">
                                        <input type="submit" onClick={handleResetPassword} value="Submit" />
                                    </div>
                                    <p className="forget" id="login">
                                        Don't have an account?
                                        <a href="/signup">Sign up</a>
                                    </p>
                                    <p className="forget" id="login">
                                        Already Have a account?
                                        <Link to="/login">Log In</Link>
                                    </p>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
}

export default ResetPassword

