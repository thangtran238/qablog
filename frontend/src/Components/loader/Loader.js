import React from 'react';
import './loader.css';

export default function Loader({ type }) {
  return (
    <div className="d-flex justify-content-center align-items-center">
      <div className="middle">
        <div className="load-bar load-bar1" />
        <div className="load-bar load-bar2" />
        <div className="load-bar load-bar3" />
        <div className="load-bar load-bar4" />
        <div className="load-bar load-bar5" />
        <div className="load-bar load-bar6" />
        <div className="load-bar load-bar7" />
        <div className="load-bar load-bar8" />
      </div>
      {type === 'loader' ? (
        <h2>Your song is uploading to the website...</h2>
      ) : null}
    </div>
  );
}
