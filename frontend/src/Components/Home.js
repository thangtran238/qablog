import { React } from "react";
import { Link } from "react-router-dom";
import Navbar from "./layout/Navbar";
import Foot from "./layout/Foot";
export default function Home() {
  return (
    <div>
      {/* <div id="preloder">
        <div className="loader" />
      </div> */}
      <Navbar></Navbar>
      <div className="hero-section">
        <div className="hero-slider owl-carousel">
          <div className="hs-item">
            <div className="container">
              <div className="row">
                <div className="col-lg-6">
                  <div className="hs-text">
                    <h2>
                      <span>Music</span> for everyone.
                    </h2>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Quis ipsum suspendisse ultrices gravida.{"{"}" "
                      {"}"}
                    </p>
                    <Link to="#" className="site-btn">
                      Download Now
                    </Link>
                    <Link to="#" className="site-btn sb-c2">
                      Start free trial
                    </Link>
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="hr-img">
                    <img src="img/hero-bg.png" alt="" />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="hs-item">
            <div className="container">
              <div className="row">
                <div className="col-lg-6">
                  <div className="hs-text">
                    <h2>
                      <span>Listen </span> to new music.
                    </h2>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Quis ipsum suspendisse ultrices gravida.{"{"}" "
                      {"}"}
                    </p>
                    <Link to="#" className="site-btn">
                      Download Now
                    </Link>
                    <Link to="#" className="site-btn sb-c2">
                      Start free trial
                    </Link>
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="hr-img">
                    <img src="img/hero-bg.png" alt="" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="intro-section spad">
        <div className="container">
          <div className="row">
            <div className="col-lg-6">
              <div className="section-title">
                <h2>Unlimited Access to 100K tracks</h2>
              </div>
            </div>
            <div className="col-lg-6">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis
                ipsum sus-pendisse ultrices gravida. Risus commodo viverra
                maecenas accumsan lacus vel facilisis. Lorem ipsum dolor sit
                amet, consectetur adipiscing elit, sed do eiusmod tempor
                incididunt ut labore et dolore magna aliqua. Quis ipsum
                suspendisse ultrices gravida.
              </p>
              <Link to="#" className="site-btn">
                Try it now
              </Link>
            </div>
          </div>
        </div>
      </div>
      <div className="how-section spad set-bg" data-setbg="img/how-to-bg.jpg">
        <div className="container text-white">
          <div className="section-title">
            <h2>How it works</h2>
          </div>
          <div className="row">
            <div className="col-md-4">
              <div className="how-item">
                <div className="hi-icon">
                  <img src="img/icons/brain.png" alt="" />
                </div>
                <h4>Create an account</h4>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipi-scing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Quis ipsum sus-pendisse ultrices gravida.{"{"}" "{"}"}
                </p>
              </div>
            </div>
            <div className="col-md-4">
              <div className="how-item">
                <div className="hi-icon">
                  <img src="img/icons/pointer.png" alt="" />
                </div>
                <h4>Choose a plan</h4>
                <p>
                  Donec in sodales dui, a blandit nunc. Pellen-tesque id eros
                  venenatis, sollicitudin neque sodales, vehicula nibh. Nam
                  massa odio, portti-tor vitae efficitur non.{"{"}" "{"}"}
                </p>
              </div>
            </div>
            <div className="col-md-4">
              <div className="how-item">
                <div className="hi-icon">
                  <img src="img/icons/smartphone.png" alt="" />
                </div>
                <h4>Download Music</h4>
                <p>
                  Ablandit nunc. Pellentesque id eros venenatis, sollicitudin
                  neque sodales, vehicula nibh. Nam massa odio, porttitor vitae
                  efficitur non, ultric-ies volutpat tellus.{"{"}" "{"}"}
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="concept-section spad">
        <div className="container">
          <div className="row">
            <div className="col-lg-6">
              <div className="section-title">
                <h2>Our Concept &amp; artists</h2>
              </div>
            </div>
            <div className="col-lg-6">
              <p>
                Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                labore et dolore magna aliqua. Quis ipsum suspendisse ultrices
                gravida. Risus commodo viverra maecenas accumsan lacus vel
                facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing
                elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                aliqua.
              </p>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3 col-sm-6">
              <div className="concept-item">
                <img src="img/concept/1.jpg" alt="" />
                <h5>Soul Music</h5>
              </div>
            </div>
            <div className="col-lg-3 col-sm-6">
              <div className="concept-item">
                <img src="img/concept/2.jpg" alt="" />
                <h5>Live Concerts</h5>
              </div>
            </div>
            <div className="col-lg-3 col-sm-6">
              <div className="concept-item">
                <img src="img/concept/3.jpg" alt="" />
                <h5>Dj Sets</h5>
              </div>
            </div>
            <div className="col-lg-3 col-sm-6">
              <div className="concept-item">
                <img src="img/concept/4.jpg" alt="" />
                <h5>Live Streems</h5>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="subscription-section spad">
        <div className="container">
          <div className="row">
            <div className="col-lg-6">
              <div className="sub-text">
                <h2>Subscription from $15/month</h2>
                <h3>Start a free trial now</h3>
                <p>
                  Consectetur adipiscing elit, sed do eiusmod tempor incididunt
                  ut labore et dolore magna aliqua. Quis ipsum suspendisse
                  ultrices gravida. Risus commodo viverra maecenas accumsan
                  lacus vel facilisis. Lorem ipsum dolor sit amet, consectetur
                  adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                  dolore magna aliqua.
                </p>
                <Link to="#" className="site-btn">
                  Try it now
                </Link>
              </div>
            </div>
            <div className="col-lg-6">
              <ul className="sub-list">
                <li>
                  <img src="img/icons/check-icon.png" alt="" />
                  Play any track
                </li>
                <li>
                  <img src="img/icons/check-icon.png" alt="" />
                  Listen offline
                </li>
                <li>
                  <img src="img/icons/check-icon.png" alt="" />
                  No ad interruptions
                </li>
                <li>
                  <img src="img/icons/check-icon.png" alt="" />
                  Unlimited skips
                </li>
                <li>
                  <img src="img/icons/check-icon.png" alt="" />
                  High quality audio
                </li>
                <li>
                  <img src="img/icons/check-icon.png" alt="" />
                  Shuffle play
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div className="concept-section spad">
        <div className="container">
          <div className="row">
            <div className="col-lg-6">
              <div className="section-title">
                <h2>Our Music Genre &amp; Artists</h2>
              </div>
            </div>
            <div className="col-lg-6">
              <p>
                Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                labore et dolore magna aliqua. Quis ipsum suspendisse ultrices
                gravida. Risus commodo viverra maecenas accumsan lacus vel
                facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing
                elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                aliqua.
              </p>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3 col-sm-6">
              <div className="concept-item">
                <img src="img/concept/1.jpg" alt="" />
                <h5>Jazz</h5>
              </div>
            </div>
            <div className="col-lg-3 col-sm-6">
              <div className="concept-item">
                <img src="img/concept/2.jpg" alt="" />
                <h5>Ballad</h5>
              </div>
            </div>
            <div className="col-lg-3 col-sm-6">
              <div className="concept-item">
                <img src="img/concept/3.jpg" alt="" />
                <h5>Pop</h5>
              </div>
            </div>
            <div className="col-lg-3 col-sm-6">
              <div className="concept-item">
                <img src="img/concept/4.jpg" alt="" />
                <h5>EDM</h5>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Foot />
    </div>
  );
}
