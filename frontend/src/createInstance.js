import axios from 'axios';
import Cookies from 'js-cookie';
import {
    jwtDecode
} from 'jwt-decode';


const REACT_APP_PORT_API = process.env.REACT_APP_PORT_API; 
const refreshToken = async () => {
    try {
        const res = await axios.post(`${REACT_APP_PORT_API}/auth/refresh`, null, {
            withCredentials: true
        });
        return res.data;
    } catch (error) {
        console.error('Token refresh failed:', error.response?.data || error.message);
        throw error;
    }
};

export const createAxios = (user, dispatch, stateSuccess) => {
    const newInstance = axios.create();

    newInstance.interceptors.request.use(async (config) => {
        let date = Date.now();
        const accessToken = Cookies.get('accesstoken');
        const decodedToken = jwtDecode(accessToken);

        if (decodedToken.exp * 1000 <= date) {
            try {
                const data = await refreshToken();
                const refreshUser = {
                    ...user,
                    accessToken: data.accessToken,
                };
                dispatch(stateSuccess(refreshUser));
                Cookies.set('accesstoken', data.accessToken);
            } catch (error) {
                console.error('Failed to refresh token:', error);
            }
        }

        return config;
    }, (err) => Promise.reject(err));

    return newInstance;
};