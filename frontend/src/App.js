import { BrowserRouter as Router, Routes, Route } from "react-router-dom";


import { NotificationProvider } from "./context/notificationProvider";
import ForgotPassword from './Components/forgotPassword/ForgotPassword';
import Login from './Components/login/Login';
import Home from "./Components/Home";
import ListSongs from "./Components/listSongs/listSongs";
import DetailMusic from "./Components/Music/DetailMusic";
import ResetPassword from './Components/resetPassword/resetPassword';
import Signup from './Components/Signup/Signup';
import MusicPostForm from "./Components/Music/MusicPostForm";
import CreatePassword from './Components/createPassword/createPassword'
function App() {
  return (
    <NotificationProvider>
      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/signup" element={<Signup />} />
          <Route path="/CreatePw/:email/:username/:tokenCreatePW" element={<CreatePassword />} />
          <Route path="/forgot-password" element={<ForgotPassword />} />
          <Route path="/resetpw/:email/:tokenResetPW" element={<ResetPassword />} />
          <Route path="/songs" element={<ListSongs />} />
          <Route path="/upload-music" element={<MusicPostForm />} />
          <Route path="/detailmusic" element={<DetailMusic />} />
        </Routes>
      </Router>
    </NotificationProvider>
  );
}

export default App;
