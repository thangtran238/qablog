import React, { useState } from 'react'
import io from "socket.io-client";
import { createContext } from 'react'

const NotificationContext = createContext();
const REACT_APP_PORT_API = process.env.REACT_APP_PORT_API; 

function NotificationProvider({ children }) {
    const socket = io.connect(`${REACT_APP_PORT_API}`);

    const [notification, setNotification] = useState([]);
    return (
        <NotificationContext.Provider value={{ socket, notification, setNotification }}>
            {children}
        </NotificationContext.Provider>
    )
}

export { NotificationContext, NotificationProvider }
