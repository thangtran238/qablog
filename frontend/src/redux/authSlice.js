import { createSlice } from "@reduxjs/toolkit";

const authSlice = createSlice({
    name: "auth",
    initialState: {
        login: {
            isFetching: false,
            currentUser: null,
            error: false
        },
        register: {
            isFetching: false,
            error: false,
            isSuccess: false
        },
        registerSendEmail: {
            isFetching: false,
            error: false,
            isSuccess: false
        },
        forgotPassword: {
            isFetching: false,
            error: false,
            isSuccess: false
        }

    },
    reducers: {
        // Login    
        loginStart: (state) => {
            state.login.isFetching = true
        },
        loginSuccess: (state, action) => {
            state.login.isFetching = false
            state.login.currentUser = action.payload
            localStorage.setItem("currentUser", JSON.stringify(action.payload));
        },
        loginFail: (state) => {
            state.login.isFetching = false
            state.login.error = true
        },

        // Logout

        logoutStart: (state) => {
            state.login.isFetching = true
        },
        logoutSuccess: (state) => {
            state.login.isFetching = false
            state.login.currentUser = null;
            localStorage.removeItem("currentUser");
        },
        logoutFail: (state) => {
            state.login.error = true
        },

        // Register
        registerStart: (state) => {
            state.register.isFetching = true
        },
        registerSuccess: (state) => {
            state.register.isFetching = false
            state.register.isSuccess = true
        },
        registerFail: (state) => {
            state.register.error = false
        },

        //registerSendEmail
        registerSendEmailStart: (state) => {
            state.registerSendEmail.isFetching = true
        },
        registerSendEmailSuccess: (state) => {
            state.registerSendEmail.isFetching = false
            state.registerSendEmail.isSuccess = true
        },
        registerSendEmailFail: (state) => {
            state.registerSendEmail.error = false
        },


        
        // forgotPassword
        forgotPasswordStart: (state) => {
            state.forgotPassword.isFetching = true;
        },
        forgotPasswordSuccess: (state) => {
            state.forgotPassword.isFetching = false;
            state.forgotPassword.isSuccess = true;
        },
        forgotPasswordFail: (state) => {
            state.forgotPassword.isFetching = false;
            state.forgotPassword.error = true;
        },

        // ResetPassword
        ResetPasswordStart: (state) => {
            state.forgotPassword.isFetching = true;
        },
        ResetPasswordSuccess: (state) => {
            state.forgotPassword.isFetching = false;
            state.forgotPassword.isSuccess = true;
        },
        ResetPasswordFail: (state) => {
            state.forgotPassword.isFetching = false;
            state.forgotPassword.error = true;
        }


    }
})


export const {
    loginStart,
    loginSuccess,
    loginFail,
    registerStart,
    registerSuccess,
    registerFail,
    registerSendEmailStart,
    registerSendEmailSuccess,
    registerSendEmailFail,
    logoutStart,
    logoutSuccess,
    logoutFail,
    forgotPasswordStart,
    forgotPasswordSuccess,
    forgotPasswordFail,
    ResetPasswordStart,
    ResetPasswordSuccess,
    ResetPasswordFail
} = authSlice.actions

export default authSlice.reducer