import { createSlice } from "@reduxjs/toolkit";

const fileSlice = createSlice({
    name: "upload",
    initialState: {
        upload: {
            isFetching: false,
            error: false,
        },
        download: {
            isFetching: false,
            error: false,
            currentFile: null
        },


    },
    reducers: {
        // Login    
        uploadStart: (state) => {
            state.upload.isFetching = true
        },
        uploadSuccess: (state) => {
            state.upload.isFetching = true
        },
        uploadFinish: (state) => {
            state.upload.isFetching = false
        },
        uploadFail: (state) => {
            state.upload.error = true
            state.upload.isFetching = false
        },


        downloadStart: (state) => {
            state.download.isFetching = true
        },
        downloadSuccess: (state) => {
            state.download.isFetching = false
        },
        downloadFail: (state) => {
            state.upload.error = true
        },


    }
})


export const {
    uploadStart,
    uploadSuccess,
    uploadFail,
    downloadStart,
    downloadSuccess,
    uploadFinish,
    downloadFail
} = fileSlice.actions

export default fileSlice.reducer