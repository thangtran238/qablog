import axios from 'axios';
import {
    ResetPasswordFail,
    ResetPasswordStart,
    ResetPasswordSuccess,
    forgotPasswordFail,
    forgotPasswordStart,
    forgotPasswordSuccess,
    loginFail,
    loginStart,
    loginSuccess,
    registerStart,
    registerSuccess,
    logoutStart,
    logoutSuccess,
    logoutFail,
    registerFail,
    registerSendEmailStart,
    registerSendEmailSuccess,
    registerSendEmailFail,
} from './authSlice';
import {
    downloadFail,
    downloadStart,
    downloadSuccess,
    uploadFail,
    uploadFinish,
    uploadStart,
    uploadSuccess
} from './fileSlice';




const REACT_APP_PORT_API = process.env.REACT_APP_PORT_API;

console.log("REACT_APP_PORT_API: ", REACT_APP_PORT_API);
export const loginUser = async (user, dispatch, navigate) => {
    dispatch(loginStart());
    try {
        const res = await axios.post(`${REACT_APP_PORT_API}/auth/signin`, user, {
            withCredentials: true
        })
        dispatch(loginSuccess(res.data));
        return {
            success: true
        }
    } catch (err) {
        dispatch(loginFail())
        if (err.response && err.response.status === 400) {
            return {
                success: false,
                error: err.response.data.message
            }
        } else if (err.response && err.response.status === 404) {
            return {
                success: false,
                error: err.response.data.message
            }
        } else {
            return {
                success: false,
                error: "Đã có lỗi xảy ra. Vui lòng thử lại sau"
            }
        }
    }
}

export const RegisterUserSendEmail = async (user, dispatch) => {
    dispatch(registerSendEmailStart());
    try {
        const res = await axios.post(`${REACT_APP_PORT_API}/auth/registerUserSendEmail`, user, {
            withCredentials: true
        })
        dispatch(registerSuccess())
        if (res.status === 200) {
            dispatch(registerSendEmailSuccess());
            return {
                success: true,
                message: res.data.message
            };
        } else {
            dispatch(registerSendEmailFail());
            return {
                success: false,
                error: "Đã có lỗi xảy ra. Vui lòng thử lại sau."
            };
        }
    } catch (err) {
        dispatch(registerSendEmailFail())
        if (err.response && err.response.status === 400) {
            return {
                success: false,
                error: err.response.data.message
            };
        } else {
            return {
                success: false,
                error: "Đã có lỗi xảy ra. Vui lòng thử lại sau."
            };
        }
    }
}

export const RegisterUser = async (user, dispatch) => {
    dispatch(registerStart());
    try {
        const res = await axios.post(`${REACT_APP_PORT_API}/auth/registerUser`, user, {
            withCredentials: true
        })
        dispatch(registerSuccess())
        if (res.status === 200) {
            dispatch(registerSuccess());
            return {
                success: true,
            };
        } else {
            dispatch(registerFail());
            return {
                success: false,
                error: "Đã có lỗi xảy ra. Vui lòng thử lại sau."
            };
        }
    } catch (err) {
        dispatch(registerFail())
        if (err.response && err.response.status === 400) {
            return {
                success: false,
                error: err.response.data.message
            };
        } else {
            return {
                success: false,
                error: "Đã có lỗi xảy ra. Vui lòng thử lại sau."
            };
        }
    }
}


export const forgotPassword = async (email, dispatch) => {
    dispatch(forgotPasswordStart());
    try {
        const res = await axios.post(`${REACT_APP_PORT_API}/auth/forgotPassword`, email, {
            withCredentials: true
        })
        const message = res.data.message;
        dispatch(forgotPasswordSuccess())
        return {
            success: true,
            message
        }
    } catch (err) {
        dispatch(forgotPasswordFail())
        if (err.response && err.response.status === 404) {
            return {
                success: false,
                message: err.response.data.message
            };
        } else if (err.response && err.response.status === 400) {
            return {
                success: false,
                error: err.response.data.message
            };
        } else {
            return {
                success: false,
                error: "Đã có lỗi xảy ra. Vui lòng thử lại sau."
            };
        }
    }
}

export const logOutUser = async (dispatch, token, navigate, axiosJWT) => {
    dispatch(logoutStart());
    try {
        await axiosJWT.post(
            `${REACT_APP_PORT_API}/auth/logout`,
            null, {
            headers: {
                authorization: `Bearer ${token}`
            },
            withCredentials: true
        }
        );
        dispatch(logoutSuccess());
        navigate('/login');
    } catch (error) {
        dispatch(logoutFail());
        console.error('Logout error:', error);
    }
};



export const resetPassword = async (resetPasswordData, dispatch) => {
    dispatch(ResetPasswordStart());
    try {
        const res = await axios.post(`${REACT_APP_PORT_API}/auth/resetPassword`, resetPasswordData, {
            withCredentials: true
        })
        const message = res.data.message;
        dispatch(ResetPasswordSuccess())
        return {
            success: true,
            message
        }
    } catch (err) {
        dispatch(ResetPasswordFail())
        if (err.response && err.response.status === 400) {
            return {
                success: false,
                error: err.response.data.message
            };
        } else {
            return {
                success: false,
                error: "Đã có lỗi xảy ra. Vui lòng thử lại sau."
            };
        }
    }
}

export const addNewMusicPost = async (dispatch, token, musicPost, navigate, axiosJWT) => {
    dispatch(uploadStart());
    try {

        const res = await axiosJWT.post(`${REACT_APP_PORT_API}/file/upload`, musicPost, {
            headers: {
                'Content-Type': 'multipart/form-data',
                'authorization': `Bearer ${token}`,
            },
            withCredentials: true
        });
        console.log(res);

        dispatch(uploadSuccess(res.data));
        dispatch(uploadFinish())
        navigate('/songs')
    } catch (error) {
        alert(error.response.data.message);
        dispatch(uploadFail());
    }
}

export const getAllMusic = async (token, axiosJWT) => {
  try {
    const res = await axiosJWT.get("http://localhost:8000/music/", {
      headers: {
        authorization: `Bearer ${token}`,
      },
      withCredentials: true,
      credentials: "include",
    });
    return res.data;
  } catch (error) {
    console.log(error);
  }
};
export const downloadMusic = async (dispatch, token, id, name, axiosJWT) => {
    dispatch(downloadStart());
    try {
        const res = await axiosJWT.get(`${REACT_APP_PORT_API}/file/download/${id}`, {
            headers: {
                'authorization': `Bearer ${token}`
            },
            withCredentials: true,
            responseType: 'blob'
        });

        if (typeof window.navigator.msSaveBlob !== 'undefined') {

            await window.navigator.msSaveBlob(res.data, `${name}.mp3`);
        } else {
            const url = window.URL.createObjectURL(new Blob([res.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', `${name}.mp3`);
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
        dispatch(downloadSuccess());

    } catch (error) {
        // console.error(error);
        dispatch(downloadFail());
    }
};

