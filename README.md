# MySoundClound

A place where people can play, download, and upload mp3 file

## Prerequisites

- npm

```node version 20.5.0```

- mongoDB

```mongodb version 6.3.0```


## Tech Stack

**Client:** ReactJS

**Server:** Node, Express, MongoDB



## Installation

1. Clone the repo

```https://gitlab.com/thangtran238/qablog/-/tree/developer?ref_type=heads```

2. Install NPM packages on server and client
- Server

```bash
cd backend
```
```bash
npm i
npm start
```

* The package.json on server side should look like this

![Server Screenshot](readmeimg/server.png)
---

- Client
```bash
cd frontend
```

```bash
npm i
npm start
```

* The package.json on client side should look like this

![Client Screenshot](readmeimg/client.png)



## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.


